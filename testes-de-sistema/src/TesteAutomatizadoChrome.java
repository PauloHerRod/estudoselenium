import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteAutomatizadoChrome {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"/home/pher/Downloads/selenium/chromedriver");

		WebDriver driver = new ChromeDriver();
		driver.get("http://www.bing.com/");
		WebElement q = driver.findElement(By.name("q"));
		q.sendKeys("Caelum");
		q.submit();
	}
}
