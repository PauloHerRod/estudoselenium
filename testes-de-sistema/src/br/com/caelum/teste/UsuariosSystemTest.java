package br.com.caelum.teste;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UsuariosSystemTest {

	private FirefoxDriver driver;
	private UsuariosPage usuarios;

	@Before
	public void inicializa() {
		driver = new FirefoxDriver();
		this.usuarios = new UsuariosPage(driver);
	}

	// @Test
	// public void deveAdicionarUmUsuario() {
	// usuarios.visita();
	// usuarios.novo().cadastra("Ronaldo Luiz de Albuquerque",
	// "ronaldo2009@terra.com.br");
	// assertTrue(usuarios.existeNaListagem("Ronaldo Luiz de Albuquerque",
	// "ronaldo2009@terra.com.br"));
	/// }

	// @Test
	// public void deveAdicionarUmUsuario() {
	// usuarios.visita();
	// usuarios.novo().cadastra("Ronaldo Luiz de Albuquerque",
	// "ronaldo2009@terra.com.br");
	// assertTrue(usuarios.existeNaListagem("Ronaldo Luiz de Albuquerque",
	// "ronaldo2009@terra.com.br"));

	// driver.get("http://localhost:8080/usuarios/new");
	// WebElement nome = driver.findElement(By.name("usuario.nome"));
	// WebElement email = driver.findElement(By.name("usuario.email"));
	//
	// nome.sendKeys("Ronaldo Luiz de Albuquerque");
	// email.sendKeys("ronaldo2009@terra.com.br");
	//
	// WebElement botaoSalvar = driver.findElement(By.id("btnSalvar"));
	// botaoSalvar.click();
	//
	// // nome.submit();
	// // email.submit();
	//
	// boolean achouNome = driver.getPageSource().contains(
	// "Ronaldo Luiz de Albuquerque");
	// boolean achouEmail = driver.getPageSource().contains(
	// "ronaldo2009@terra.com.br");
	//
	// assertTrue(achouNome);
	// assertTrue(achouEmail);
	// }

	// @Test
	// public void naoDeveAdicionarUmUsuarioSemNome() {
	// driver.get("http://localhost:8080/usuarios/new");
	//
	// WebElement email = driver.findElement(By.name("usuario.email"));
	//
	// email.sendKeys("ronaldo2009@terra.com.br");
	// email.submit();
	//
	// assertTrue(driver.getPageSource().contains("Nome obrigatorio!"));
	// }
	//
	// @Test
	// public void naoDeveAdicionarUmUsuarioSemNomeOuEmail() {
	// driver.get("http://localhost:8080/usuarios/new");
	//
	// WebElement email = driver.findElement(By.name("usuario.email"));
	// email.submit();
	//
	// assertTrue(driver.getPageSource().contains("Nome obrigatorio!"));
	// assertTrue(driver.getPageSource().contains("E-mail obrigatorio!"));
	///}

	@Test
	public void deveDeletarUmUsuario() {
		usuarios.visita();
		usuarios.novo().cadastra("Ronaldo Luiz de Albuquerque",
				"ronaldo2009@terra.com.br");
		assertTrue(usuarios.existeNaListagem("Ronaldo Luiz de Albuquerque",
				"ronaldo2009@terra.com.br"));

		usuarios.deletaUsuarioNaPosicao(2);

		assertFalse(usuarios.existeNaListagem("Ronaldo Luiz de Albuquerque",
				"ronaldo2009@terra.com.br"));

	}

	@After
	public void finaliza() {
		driver.close();
	}
}
