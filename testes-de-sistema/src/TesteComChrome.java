import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class TesteComChrome {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", 
                "/opt/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://www.google.com");
		
		WebElement campoTexto = driver.findElement(By.name("q"));
		campoTexto.sendKeys("caelum");
		campoTexto.submit();
	}

}
